var printer = require("printer");
var current_printer = "Zebra_LP2824_ZPL___infobox_pc";

function availablePrinterNames() {
    return printer.getPrinters().map(x => x.name);
}

function main() {

    (function displayPrinterNames() {
        console.log("Printers:");
        availablePrinterNames().forEach(name => console.log(`  ${name}`))
    })();

    (function print() {

        var template ="N\nS4\nD15\nq400\nR\nB20,10,0,1,2,30,173,B,\"barcode\"\nP0\n";

        printer.printDirect({
            data: template,
            printer: current_printer,
            type: "RAW",
            success: function () {
                console.log("printed: nothing");
                console.log(template);
            },
            error: function (err) {
                console.log(err);
            }
        });

    })();
}

main();